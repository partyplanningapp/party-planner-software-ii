//
//  HNoteDetailViewController.swift
//  Party Planner Pro
//
//  Created by Janie on 10/3/16.
//  Copyright © 2016 software2. All rights reserved.
//

import UIKit

class HNoteDetailViewController: UIViewController {

    var note: HalloweenNote!
    

    @IBOutlet weak var htitleTextField: UITextField!
    @IBOutlet weak var hcontentTextField: UITextView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        htitleTextField.text = note.title
        hcontentTextField.text = note.content
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        note.title = htitleTextField.text!
        note.content = hcontentTextField.text
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
