//
//  BNoteDetailViewController.swift
//  Party Planner Pro
//
//  Created by Janie on 10/3/16.
//  Copyright © 2016 software2. All rights reserved.
//

import UIKit

class BNoteDetailViewController: UIViewController {
    
    var note: BirthdayNote!

    @IBOutlet weak var btitleTextField: UITextField!
    @IBOutlet weak var bcontentTextField: UITextView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        btitleTextField.text = note.title
        bcontentTextField.text = note.content
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        note.title = btitleTextField.text!
        note.content = bcontentTextField.text
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
